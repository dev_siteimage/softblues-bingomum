<?php namespace ResponsiveMenu;

use Premmerce\SDK\V2\FileManager\FileManager;
use ResponsiveMenu\Admin\Admin;
use ResponsiveMenu\Frontend\Frontend;

/**
 * Class ResponsiveMenuPlugin
 *
 * @package ResponsiveMenu
 */
class ResponsiveMenuPlugin
{
    /**
     * @var FileManager
     */
    private $fileManager;

    /**
     * ResponsiveMenuPlugin constructor.
     *
     * @param string $mainFile
     */
    public function __construct( $mainFile )
    {
        $this->fileManager = new FileManager( $mainFile );

        add_action( 'plugins_loaded', [$this, 'loadTextDomain'] );
        add_action( 'init', [$this, 'registerShortcode'] );
    }

    /**
     * Run plugin part
     */
    public function run()
    {
        if ( is_admin() ) {
            new Admin( $this->fileManager );
        } else {
            new Frontend( $this->fileManager );
        }
    }

    /**
     * Load plugin translations
     */
    public function loadTextDomain()
    {
        $name = $this->fileManager->getPluginName();
        load_plugin_textdomain( 'responsive-menu-addon', false, $name . '/languages/' );
    }

    /**
     * Register shortcode
     */
    public function registerShortcode()
    {
        add_shortcode('menus_icons', function ($atts = [], $content = null) {
            $atts = array_unique( explode( ',', $atts['options'] ) );
            return $this->fileManager->renderTemplate('frontend/shortcode.php', array(
              'atts' => $atts,
            ));
        });
    }

    /**
     * Fired when the plugin is activated
     */
    public function activate()
    {
        // TODO: Implement activate() method.
    }

    /**
     * Fired when the plugin is deactivated
     */
    public function deactivate()
    {
        // TODO: Implement deactivate() method.
    }

    /**
     * Fired during plugin uninstall
     */
    public static function uninstall()
    {
        // TODO: Implement uninstall() method.
    }
}
<?php namespace ResponsiveMenu\Frontend;

use Premmerce\SDK\V2\FileManager\FileManager;
use ResponsiveMenu\Admin\Admin;

/**
 * Class Frontend
 *
 * @package ResponsiveMenu\Frontend
 */
class Frontend
{
    /**
     * @var FileManager
     */
    private $fileManager;

    /**
     * @var string
     */
    private $settingsHandle;

    public function __construct( FileManager $fileManager )
    {
        $this->fileManager    = $fileManager;
        $this->settingsHandle = 'responsive-menu-frontend';

        add_action( 'wp_enqueue_scripts', array($this, 'enqueueScripts'), 1 );
    }

    /**
     * Enqueue scripts
     */
    public function enqueueScripts()
    {
        wp_enqueue_style(
          $this->settingsHandle . '-css',
          $this->fileManager->locateAsset( 'frontend/css/style.css' ),
          array(),
          Admin::VERSION
        );

        $cssFile = $this->fileManager->getPluginDirectory() . 'assets/frontend/css/style.css';
        if( file_exists( $cssFile ) ) { $dynamicCss = file_get_contents( $cssFile ); }
        if( ! $dynamicCss ) { return; }
        $pattern    = '#(.+)\$([a-z_0-9]+)(\[?([a-z_]*)\]?)(\{(.+)\})?(.+)#';
        $dynamicCss = preg_replace_callback( $pattern, array($this, 'parseDynamicCssOptions'), $dynamicCss );

        wp_add_inline_style(
          $this->settingsHandle . '-css',
          $dynamicCss
        );
    }

    /**
     * Parse dynamic css options
     * 
     * @param array $opts
     * @return string
     */
    public function parseDynamicCssOptions( $opts ) {
        $result = $opts[1];
        $option = get_option( 'responsive_menu_options_data', [] );
        $option = $option['color'][$opts[2]];

        if ( $option ) {
            $result .= $option;
        } else {
            return $result = '';
        }

        return $result . $opts[6] . $opts[7];
    }
}
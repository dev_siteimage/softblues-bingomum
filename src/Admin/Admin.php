<?php namespace ResponsiveMenu\Admin;

use Premmerce\SDK\V2\FileManager\FileManager;

/**
 * Class Admin
 *
 * @package ResponsiveMenu\Admin
 */
class Admin
{
    /**
     * @var FileManager
     */
    private $fileManager;

    /**
     * @var string
     */
    private $settingsPage;

    const VERSION = '1.0';
    const AJAX_GET_OPTION_ITEM = 'get_option_item';

    /**
     * Admin constructor.
     *
     * Register menu items and handlers
     *
     * @param FileManager $fileManager
     */
    public function __construct( FileManager $fileManager )
    {
        $this->fileManager = $fileManager;
        $this->settingsPage = 'responsive-menu-admin';

        add_action( 'admin_menu', array($this, 'addMenuPage') );
        add_action( 'admin_init', array($this, 'registerSettings') );

        add_action( 'admin_enqueue_scripts', array($this, 'enqueueAdminScripts') );
        add_action( 'wp_ajax_' . self::AJAX_GET_OPTION_ITEM, array($this, 'ajaxGetItemHtml') );
    }

    /**
     * Enqueue admin scripts
     */
    public function enqueueAdminScripts()
    {
        wp_enqueue_script( 'wp-color-picker' );
        wp_enqueue_style( 'wp-color-picker' );

        wp_enqueue_style(
          $this->settingsPage . '-css',
          $this->fileManager->locateAsset( 'admin/css/style.css' ),
          array(),
          self::VERSION
        );

        wp_enqueue_script(
          $this->settingsPage . '-js',
          $this->fileManager->locateAsset( 'admin/js/common.js' ),
          array('jquery'),
          self::VERSION
        );
        $wpData = array(
          'ajaxUrl' => admin_url( 'admin-ajax.php' ),
        );
        wp_localize_script(
          $this->settingsPage . '-js',
          'wpData',
          $wpData
        );
    }

    public function ajaxGetItemHtml()
    {
        $key = ( ! empty( $_POST['id'] ) ) ? $_POST['id'] : 0;
        echo $this->fileManager->includeTemplate( 'admin/section/option-item.php', array(
          'key' => $key,
        ));
        die();
    }

    /**
     * Add submenu
     */
    public function addMenuPage()
    {
        global $admin_page_hooks;

        $responsiveMenuExists = isset( $admin_page_hooks['responsive_menu_addon'] );

        if ( ! $responsiveMenuExists ) {
            add_menu_page(
              esc_html__( 'Responsive Menu' ),
              esc_html__( 'Responsive Menu' ),
              'manage_options',
              'responsive_menu_addon',
              '',
              'dashicons-list-view'
            );
        }

        add_submenu_page(
          'responsive_menu_addon',
          esc_html__( 'All Options', 'responsive-menu-addon' ),
          esc_html__( 'All Options', 'responsive-menu-addon' ),
          'manage_options',
          $this->settingsPage,
          array($this, 'options')
        );

        if ( ! $responsiveMenuExists ) {
            global $submenu;
            unset( $submenu['responsive_menu_addon'][0] );
        }
    }

    /**
     * Options page
     */
    public function options()
    {
        if ( isset( $_POST['update-responsive-menu-options'] ) ) {
            $this->update();
        }

        $this->fileManager->includeTemplate( 'admin/options.php', array(
          'pageSlug' => $this->settingsPage,
        ) );
    }

    /**
     * Register plugin settings
     */
    public function registerSettings()
    {
        add_settings_section( 'responsive-menu-settings', '', array($this, 'optionsCallback'), $this->settingsPage );
        register_setting( 'responsive_menu_addon_options', 'responsive_menu_addon_options_item' );
    }

    public function optionsCallback()
    {
        $options = get_option( 'responsive_menu_options_data', [] );
        $this->fileManager->includeTemplate( 'admin/section/color-picker.php', array(
          'options' => $options,
        ) );

        $this->fileManager->includeTemplate( 'admin/section/option-list.php', array(
          'options' => $options,
        ) );
    }

    public function update()
    {
        $result = [];

        foreach ( $_POST['color'] as $key => $item ) {
            $result['color'][$key] = esc_attr( $item );
        }

        foreach ( $_POST['options'] as $key => $item ) {
            if ( ! empty( $item ) ) {
                if ( $item['link'] != '' || $item['icon'] != '' || $item['title'] != '' ) {
                    $result['options'][] = [
                      'link'  => esc_url_raw( $item['link'] ),
                      'icon'  => esc_attr( $item['icon'] ),
                      'title' => esc_attr( $item['title'] ),
                    ];
                }
            }
        }

        update_option( 'responsive_menu_options_data', $result );
    }
}
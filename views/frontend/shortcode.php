<?php

if ( ! defined( 'WPINC' ) ) {
    die;
}

/**
 * @var string $atts
 */

$options = get_option( 'responsive_menu_options_data', [] );

?>

<ul class="responsive-menu">

<?php
    if( ! empty( $options['options'] ) ) {
        foreach ( $options['options'] as $key => $option ) {
            if( in_array( $key+1, $atts ) ) {
                $icon = !empty( $option['icon'] ) ? '<i class="'.$option['icon'].'"></i>' : '';
                echo "<li class='responsive-menu__item'>";
                    echo sprintf(
                      '<a class="responsive-menu__link" href="%1$s"><span class="responsive-menu__icon">%2$s</span><span class="responsive-menu__title">%3$s</span></a>',
                      $option['link'],
                      $icon,
                      $option['title']
                    );
                echo "</li>";
            }
        }
    }
?>

</ul>
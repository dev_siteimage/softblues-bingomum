<?php

if ( ! defined( 'WPINC' ) ) {
    die;
}

/**
 *
 * @var string $pageSlug
 *
 */

?>

<div class="wrap">
    <h2><?php esc_html_e('Responsive Menu Options', 'responsive-menu-addon'); ?></h2><br>

    <table class="form-table" role="presentation">
        <tbody>
        <tr>
            <th scope="row"><?php esc_html_e('Shortcode: ', 'responsive-menu-addon'); ?></th>
            <td>
                <p class="description"><b>[menus_icons options="1,2,3,4"]</b></p>
            </td>
        </tr>
        </tbody>
    </table>

    <div class="form-wrap">
        <form id="responsive_menu_addon_form" method="post">
            <?php
                settings_fields( 'responsive_menu_addon_options' );
                do_settings_sections( $pageSlug );

                ?>
                    <input type="submit" name="add_new_option" id="add_new_option" class="button" value="<?php esc_html_e( 'Add new option', 'responsive-menu-addon' ); ?>">
                <?php

                submit_button( esc_html__( 'Update', 'responsive-menu-addon' ), 'primary', 'update-responsive-menu-options', true, array( 'value' => '1' ) );
            ?>
        </form>
    </div>

</div>
<?php

if ( ! defined( 'WPINC' ) ) {
    die;
}

/**
 * @var array $options
 */

if( ! empty( $options['options'] ) ) {
    foreach ( $options['options'] as $key => $option ) {
        ?>
        <div class="option-wrapp" data-id="<?php echo $key+1; ?>">
            <h4><?php esc_html_e( 'Option #', 'responsive-menu-addon' ); echo $key+1; ?></h4>

            <input type="text" name="<?php echo 'options['.$key.'][link]'; ?>" value="<?php echo $option['link']; ?>" placeholder="<?php esc_html_e( 'Link URL', 'responsive-menu-addon' ); ?>"><br>
            <input type="text" name="<?php echo 'options['.$key.'][icon]'; ?>" value="<?php echo $option['icon']; ?>" placeholder="<?php esc_html_e( 'Icon URL', 'responsive-menu-addon' ); ?>"><br>
            <input type="text" name="<?php echo 'options['.$key.'][title]'; ?>" value="<?php echo $option['title']; ?>" placeholder="<?php esc_html_e( 'Title', 'responsive-menu-addon' ); ?>">
        </div>
        <?php
    }
}
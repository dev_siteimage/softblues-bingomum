<?php

if ( ! defined( 'WPINC' ) ) {
    die;
}

/**
 * @var array $key
 */

?>

<div class="option-wrapp" data-id="<?php echo $key; ?>">
    <h4><?php esc_html_e( 'Option #', 'responsive-menu-addon' ); echo $key; ?></h4>

    <input type="text" name="<?php echo 'options['.$key.'][link]'; ?>" value="" placeholder="<?php esc_html_e( 'Link URL', 'responsive-menu-addon' ); ?>"><br>
    <input type="text" name="<?php echo 'options['.$key.'][icon]'; ?>" value="" placeholder="<?php esc_html_e( 'Icon URL', 'responsive-menu-addon' ); ?>"><br>
    <input type="text" name="<?php echo 'options['.$key.'][title]'; ?>" value="" placeholder="<?php esc_html_e( 'Title', 'responsive-menu-addon' ); ?>">
</div>
<?php

if ( ! defined( 'WPINC' ) ) {
    die;
}

/**
 * @var array $options
 */

?>

<div class="color-wrapp">
    <span><?php esc_html_e( 'Icons color', 'responsive-menu-addon' ); ?></span>
    <input type="text" name="<?php echo 'color[icon]'; ?>" value="<?php echo $options['color']['icon']; ?>">

    <span><?php esc_html_e( 'Background color', 'responsive-menu-addon' ); ?></span>
    <input type="text" name="<?php echo 'color[bg]'; ?>" value="<?php echo $options['color']['bg']; ?>">

    <span><?php esc_html_e( 'Hover effect', 'responsive-menu-addon' ); ?></span>
    <input type="text" name="<?php echo 'color[hover]'; ?>" value="<?php echo $options['color']['hover']; ?>">
</div>
=== Responsive Menu Addon ===
Contributors: admin, premmerce
Tags: tags
Requires at least: 5.6
Tested up to: 5.6
Stable tag: 1.0
Requires PHP: 5.6
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html



== Description ==


== Installation ==

1. Unzip the downloaded zip file.
1. Upload the plugin folder into the `wp-content/plugins/` directory of your WordPress site.
1. Activate `Responsive Menu Addon` from Plugins page

== Changelog ==

= 1.0 =

Release Date: Jan 11, 2021

* Initial release
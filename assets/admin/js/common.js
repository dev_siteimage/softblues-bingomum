(function ($) {
    $(document).ready(function () {

        $('#add_new_option').on('click', function(event) {
            event.preventDefault();
            let id = $(this).closest('#responsive_menu_addon_form').find('.option-wrapp').last().data('id');

            $.ajax({
                type : "POST",
                url  : wpData.ajaxUrl,
                data : {
                    action: 'get_option_item',
                    id    : id + 1
                },
                success : function (data) {
                    $('#responsive_menu_addon_form #add_new_option').before(data);
                }
            });
        });

        if( $('input[name*="color"]').length ) {
            $('input[name*="color"]').wpColorPicker();
        }

    });
})(jQuery);